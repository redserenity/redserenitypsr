<?php

namespace RedSerenity\Psr\Cache;

/**
 * Exception interface for all exceptions thrown by an Implementing Library.
 *
 * Interface CacheException
 */
interface CacheException
{

}
