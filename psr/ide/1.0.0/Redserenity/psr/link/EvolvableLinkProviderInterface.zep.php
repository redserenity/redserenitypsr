<?php

namespace RedSerenity\Psr\Link;

/**
 * An evolvable link provider value object.
 */
interface EvolvableLinkProviderInterface extends \RedSerenity\Psr\Link\LinkProviderInterface
{

    /**
     * Returns an instance with the specified link included.
     *
     * If the specified link is already present, this method MUST return normally
     * without errors. The link is present if $link is === identical to a link
     * object already in the collection.
     *
     * @param LinkInterface $link
     *   A link object that should be included in this collection.
     * @param LinkInterface $link
     * @return EvolvableLinkProviderInterface
     */
    public function withLink(LinkInterface $link);

    /**
     * Returns an instance with the specifed link removed.
     *
     * If the specified link is not present, this method MUST return normally
     * without errors. The link is present if $link is === identical to a link
     * object already in the collection.
     *
     * @param LinkInterface $link
     *   The link to remove.
     * @param LinkInterface $link
     * @return EvolvableLinkProviderInterface
     */
    public function withoutLink(LinkInterface $link);

}
