<?php

namespace RedSerenity\Psr\Container;

/**
 * Base interface representing a generic exception in a container.
 *
 * Interface ContainerExceptionInterface
 */
interface ContainerExceptionInterface
{

}
