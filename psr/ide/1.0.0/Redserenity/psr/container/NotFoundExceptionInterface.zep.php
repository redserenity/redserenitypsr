<?php

namespace RedSerenity\Psr\Container;

/**
 * No entry was found in the container.
 *
 * Interface NotFoundExceptionInterface
 */
interface NotFoundExceptionInterface extends \RedSerenity\Psr\Container\ContainerExceptionInterface
{

}
