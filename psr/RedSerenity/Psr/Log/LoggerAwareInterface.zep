namespace RedSerenity\Psr\Log;

/**
 * Describes a logger-aware instance.
 *
 * Interface LoggerAwareInterface
 */
interface LoggerAwareInterface {
    /**
     * Sets a logger instance on the object.
     *
     * @param LoggerInterface logger
     * @return void
     */
    public function setLogger(<LoggerInterface> logger) -> void;
}
